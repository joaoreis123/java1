package java1;

import java.util.Scanner;

/**
 *
 * @author Reis
 */
public class Java1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
      
        float N, num, conta, soma,somaNeg, contaNeg;
        float perc_positivos, media, mediaNeg;
        
        Scanner ler = new Scanner (System.in);
        System.out.println("Quantos numeros quer ler:");
        N = ler.nextInt();
        conta = 0;
        soma = 0;
        somaNeg=0;
        contaNeg=0;
        
        do  {
            N = ler.nextInt();
        } while (N<0);
        
        System.out.println("");
        
        for (int i=1; i<N+1; i++){
            System.out.println("Insira um numero");
            num= ler.nextInt();
            soma=soma+num;
            if (num>0){
                conta++;
            }
            if (num<0){
                contaNeg++;
                somaNeg=somaNeg+num;
            }
                
        }        
        mediaNeg=somaNeg/contaNeg;
        media=soma/N;
        perc_positivos = (conta/N)*100;
            System.out.printf("A percentagem de positivos é: %.2f", perc_positivos);
            System.out.println("");
            System.out.printf("A media dos numeros lidos é: %.2f", media );
            System.out.println("");
            System.out.printf("A soma dos numeros negativos é: %.2f", somaNeg );
            System.out.println("");
            System.out.printf("A media dos numeros negativos é: %.2f", mediaNeg );
    }
       
}
